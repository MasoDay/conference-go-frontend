import MenuItem from './MenuItem'
import './Nav.css'


function Nav() {

    const links = ["home", "new location", "new conference", "new presentation", "logout"]

    return (
        <div className='navbar'>


        {
            links.map( (link, key ) =>
                <MenuItem link={link} key={key} />)
        }

    </ div>
    );
}

export default Nav;
