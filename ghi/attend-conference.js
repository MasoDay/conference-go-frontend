window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const attendeesUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(attendeesUrl, fetchConfig)
        if (response.ok){
            formTag.reset();
            const alertTag = document.getElementById('success-message')
            alertTag.classList.remove('d-none')
            formTag.classList.add('d-none')
        }
    })
    if (response.ok) {
      const data = await response.json();
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      const loadingConferenceSpinner = document.getElementById('loading-conference-spinner')
      loadingConferenceSpinner.classList.add('d-none')
      selectTag.classList.remove("d-none")
    }
  });


// window.addEventListener('DOMContentLoaded', async () => {
//     const selectTag = document.getElementById('conference');

//     const url = 'http://localhost:8000/api/conferences/';
//     const response = await fetch(url);

//     const formTag = document.getElementById('create-presentation-form')
//     formTag.addEventListener('submit', async event => {
//         event.preventDefault();
//         const formData = new FormData(formTag);
//         const json = JSON.stringify(Object.fromEntries(formData));
//         const conferenceUrl = JSON.parse(json).conference;
//         const presentationUrl = `http://localhost:8000${conferenceUrl}presentations/`;
//         const fetchConfig = {
//             method: "post",
//             body: json,
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//         };

//         const response = await fetch(presentationUrl, fetchConfig);

//         if (response.ok) {
//             formTag.reset();
//             const newPresentation = await response.json();
//             console.log(newPresentation);
//             alertTag.classList.remove('d-none');
//         } else {
//             console.error("response was not ok")
//         }
//     });

//     if (response.ok) {
//       const data = await response.json();

//       for (let conference of data.conferences) {
//         const option = document.createElement('option');
//         option.value = conference.href;
//         option.innerHTML = conference.name;
//         selectTag.appendChild(option);
//       }
//       const spinner = document.getElementById('loading-conference-spinner')
//       spinner.classList.add('d-none');
//       selectTag.classList.remove('d-none');
//     }
//     const alertTag = document.getElementById('success-message');
//   });

//   const formTag = document.getElementById("create-attendee-form");
//   formTag.addEventListener("submit", async event => {
//     event.preventDefault();
//     const formData = new FormData(formTag);
//     const json = JSON.stringify(Object.fromEntries(formData));
//     const attendeesUrl = "http://localhost:8001/api/attendees";
//     const fetchConfig = {
//         method: "post",
//         body: json,
//         headers: {
//             "content-type": "application/json"
//         }

//     }
//     const response = await fetch(attendeesUrl, fetchConfig);
//     if (response.ok){
//         formData.reset();
//     }
//   });
